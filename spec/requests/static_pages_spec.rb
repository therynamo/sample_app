require 'spec_helper'

# Visits home page and makes sure that there is
# content on the page that says 'Sample App' somewhere
describe "Static Pages" do

	let(:base_title){"Ruby on Rails Tutorial Sample App"}

	describe "Home page" do
		it "should have content 'Sample App'" do
			visit '/static_pages/home'
			expect(page).to have_content('Sample App')
		end

		it "should have the base title 'Home'" do
			visit '/static_pages/home'
			expect(page).to have_title("#{base_title}")
		end
		it "should not have the custom title"  do
			visit '/static_pages/home'
			expect(page).not_to have_title("| Home")
		end
	end

	# Visit the Help page and make sure relevant content is available
	# for the user

	describe "Help page" do
		it "should have content 'Help'" do
			visit '/static_pages/help'
			expect(page).to have_content('Help')
		end

		 it "should have the title 'Help'" do
	        visit '/static_pages/help'
	        expect(page).to have_title("#{base_title} | Help")
	    end
	end

	#Visits and ensures that 'about' content is given for the user

	describe "About page" do
		it "should have content 'About Us'" do
			visit '/static_pages/about'
			expect(page).to have_content('About Us')
		end
		 
		it "should have the title 'About Us'" do
	      visit '/static_pages/about'
	      expect(page).to have_title("#{base_title} | About Us")
	  end
	end


	#Visits contact page
	describe "Contact page" do
		it "should have content 'Contact'" do
			visit '/static_pages/contact'
			expect(page).to have_content('Contact')
		end

		it "should have title 'Contact'" do
			visit '/static_pages/contact'
			expect(page).to have_title("#{base_title} | Contact")
		end

	end
end


